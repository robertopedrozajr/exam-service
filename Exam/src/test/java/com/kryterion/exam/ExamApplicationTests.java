package com.kryterion.exam;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.kryterion.exam.client.rabbitmq.service.ExamClientRabbitMQSendRequestService;

@SpringBootTest
class ExamApplicationTests {

	@Test
	void contextLoads() {
		
		
		
		
	}
	
	@Test
	void testException() {
		
		ExamClientRabbitMQSendRequestService eC = new ExamClientRabbitMQSendRequestService();
		
		List<Integer> clientIDs = new ArrayList<Integer>();
		
		clientIDs.add(1);
		clientIDs.add(9);
		clientIDs.add(0);
		clientIDs.add(-22333);
		
		eC.sendRequestClientIDs(clientIDs);
		
	}

}
