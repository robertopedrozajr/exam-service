package com.kryterion.exam.client.rabbitmq.service;

import java.util.List;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kryterion.exam.client.rabbitmq.config.ExamClientRabbitMQConfiguration;
import com.kryterion.exam.model.ExamClientRequestRabbitMQModel;

@Service
public class ExamClientRabbitMQSendRequestService {
	
	@Autowired
	RabbitTemplate rabbitTemplate;
	
	public void sendRequest(List<ExamClientRequestRabbitMQModel> examClientRequestRabbitMQModelList) {
		
		rabbitTemplate.convertAndSend(ExamClientRabbitMQConfiguration.EXCHANGE, ExamClientRabbitMQConfiguration.ROUTING_KEY_EXAM_CLIENT, examClientRequestRabbitMQModelList);
		
	}
	
	public void sendRequestClientIDs(List<Integer> clientIDsList) {
		
		rabbitTemplate.convertAndSend(ExamClientRabbitMQConfiguration.EXCHANGE, ExamClientRabbitMQConfiguration.ROUTING_KEY_EXAM_CLIENT, clientIDsList);
		
	}

}
