package com.kryterion.exam.client.rabbitmq.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ExamClientRabbitMQConfiguration {
	
	public static final String EXCHANGE = "exam_Client_Exchange";
	public static final String DEAD_LETTER_EXCHANGE = "deadLetterExchange";
	
	
	public static final String ROUTING_KEY_EXAM_CLIENT = "queue.examClient";
	public static final String ROUTING_KEY_CLIENT_EXAM = "queue.clientExam";
	public static final String ROUTING_KEY_DEAD_LETTER = "deadLetter";
	
	public static final String QUEUE_EXAM_CLIENT = "exam_Client_Queue";
	
	public static final String QUEUE_CLIENT_EXAM = "client_Exam_Que"; // Reserved
	
	public static final String QUEUE_DEAD_LETTER = "dead_Letter_Que"; // Reserved
	
	
	/* CONVERTER */
	@Bean
	public MessageConverter converter() {
		
		return new Jackson2JsonMessageConverter();
	}
	
	/* QUEUE */
	@Bean
	public Queue examClientQueue() {
		
		return QueueBuilder.durable(QUEUE_EXAM_CLIENT).withArgument("x-dead-letter-exchange", DEAD_LETTER_EXCHANGE)
				.withArgument("x-dead-letter-routing-key", ROUTING_KEY_DEAD_LETTER).build();
	}
	
	@Bean
	public Queue clientExamQueue() {
		
		return new Queue(QUEUE_CLIENT_EXAM);
	} 
	
	/*
	@Bean
	public Queue clientExamQueue() {
		
		return new Queue(QUEUE_CLIENT_EXAM);
	} */
	
	@Bean
	public Queue dlq() {
		
		return QueueBuilder.durable(QUEUE_DEAD_LETTER).build();
	}
	
	/* EXCHANGE */
	@Bean
	public TopicExchange exchange() {
		
		return new TopicExchange(EXCHANGE);
		
	}
	
	@Bean
	public TopicExchange deadLetterExchange() { /* DLQ */
		
		return new TopicExchange(DEAD_LETTER_EXCHANGE);
		
	}
	
	
	/* BINDING */
	@Bean
	public Binding DLQBinding() {
		
		return BindingBuilder.bind(dlq()).to(deadLetterExchange()).with(ROUTING_KEY_DEAD_LETTER);
	}
	
	@Bean
	public Binding bindingExamClientQueue(TopicExchange exchange) {
		
		return BindingBuilder.bind(examClientQueue()).to(exchange).with(ROUTING_KEY_EXAM_CLIENT);
		
	}
	
	@Bean
	public Binding bindingClientExamQueue(TopicExchange exchange) {
		
		return BindingBuilder.bind(clientExamQueue()).to(exchange).with(ROUTING_KEY_CLIENT_EXAM);
		
	}
	
}
