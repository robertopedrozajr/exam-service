package com.kryterion.exam.model;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class ExamPaginationRequestModel {
	
	int pageNumber;
	int pageSize;
	String sortedBy;
	String ascDesc;
	public int getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public String getSortedBy() {
		return sortedBy;
	}
	public void setSortedBy(String sortedBy) {
		this.sortedBy = sortedBy;
	}
	public String getAscDesc() {
		return ascDesc;
	}
	public void setAscDesc(String ascDesc) {
		this.ascDesc = ascDesc;
	}

}
