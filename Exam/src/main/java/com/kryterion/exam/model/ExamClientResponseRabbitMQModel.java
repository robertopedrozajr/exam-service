package com.kryterion.exam.model;

import javax.persistence.Column;

import org.springframework.stereotype.Component;

@Component
public class ExamClientResponseRabbitMQModel {

	/*
	private long messageRequestID;
	public long getMessageRequestID() {
		return messageRequestID;
	}
	public void setMessageRequestID(long messageRequestID) {
		this.messageRequestID = messageRequestID;
	}
	*/
	
	@Column(name = "client_ID", nullable = false)
	private Integer clientId;
	
	@Column(name = "client_name", nullable = false)
	private String clientName;
	
	public Integer getClientId() {
		return clientId;
	}
	public void setClientId(Integer clientID) {
		this.clientId = clientID;
	}
	
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	
}
