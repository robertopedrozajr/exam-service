package com.kryterion.exam.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Temporal;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.stereotype.Service;


@Service
public class ExamResponseModelWithClientNames {
	
	String pattern = "MM-dd-yyyy HH:mm:ss aa";
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

	
	@Column(name = "exam_ID",  unique = true)
	private int examId;
	
	@Column(name = "client_ID", nullable = false)
	private int clientId; // client(FK)
	
	@Column(name = "client_name", nullable = true)
	private String clientName = "";
	
	@Column(name = "exam_name", length = 80, nullable = false)
	private String examName;
	
	@Column(name = "exam_type", length = 15, nullable = false)
	private String examType;
	
	@CreationTimestamp
	@Column(name = "created_date", updatable = false)
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date createdDate;
	
	@UpdateTimestamp
	@Column(name = "modified_date", nullable = true, updatable = true)
	private Date modifiedDate;
	
	// Additional Jan 15 2022
		@Column(name = "deleted_flag", length = 3)
		private int deletedFlag = 0;
		public int getDeletedFlag() {
			return deletedFlag;
		}
		public void setDeletedFlag(int deletedFlag) {
			this.deletedFlag = deletedFlag;
		}

	public int getExamId() {
		return examId;
	}

	public void setExamId(int examID) {
		this.examId = examID;
	}

	public int getClientID() {
		return clientId;
	}

	public void setClientID(int clientID) {
		this.clientId = clientID;
	}
	
	// Client Name
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	// Client Name
	
	public String getExamName() {
		return examName;
	}

	public void setExamName(String examName) {
		this.examName = examName;
	}

	public String getExamType() {
		return examType;
	}

	public void setExamType(String examType) {
		this.examType = examType;
	}

	public String getCreatedDate() {
		
		return simpleDateFormat.format(createdDate);
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedDate() {
		return simpleDateFormat.format(createdDate);
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

}
