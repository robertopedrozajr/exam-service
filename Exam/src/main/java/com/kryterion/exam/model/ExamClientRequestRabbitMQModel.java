package com.kryterion.exam.model;

import javax.persistence.Column;

import org.springframework.stereotype.Component;

@Component
public class ExamClientRequestRabbitMQModel {
	

	/*
	private long messageRequestID;
	public long getMessageRequestID() {
		return messageRequestID;
	}
	public void setMessageRequestID(long messageRequestID) {
		this.messageRequestID = messageRequestID;
	}
	*/
	
	@Column(name = "client_ID", nullable = false)
	private Integer clientId;
	
	public Integer getClientId() {
		return clientId;
	}

	public void setClientId(Integer clientID) {
		this.clientId = clientID;
	}

}
