package com.kryterion.exam.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.modelmapper.spi.MappingContext;
//import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.kryterion.exam.entity.ExamEntity;
import com.kryterion.exam.model.ExamRequestUpdateModel;
import com.kryterion.exam.model.ExamResponseModelWithClientNames;
import com.kryterion.exam.model.ExamResponseUpdateModel;
import com.kryterion.exam.repository.ExamRepository;
import com.kryterion.exam.repository.ExamRepositoryPageable;

@Service
public class ExamService {
	
	@Autowired
	ExamRepository examRepository;
	
	@Autowired
	ExamRepositoryPageable examRepositoryPageable;
	
	//@Autowired
	//ExamResponseModelWithClientNames examResponseModelWithClientNames;
	
	//Logger logger = LoggerFactory.getLogger(this.getClass());
	
	
 	public ExamEntity addExam(ExamEntity examEntity) {
		
		ExamEntity examEntityReturn = examRepository.save(examEntity);
		
		return examEntityReturn;
		
	}
	
	public ExamResponseUpdateModel updateExam(ExamRequestUpdateModel examRequestUpdateModel) {
	  
		ExamResponseUpdateModel examResponseUpdateModel = null;
		
		if(this.findExamById(examRequestUpdateModel.getExamID()) != null) {
			ModelMapper modelMapper = new ModelMapper();
			modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT); // should match strictly
			ExamEntity updateEntityReturn = modelMapper.map(examRequestUpdateModel, ExamEntity.class);
			
			
			try {
				
				updateEntityReturn = examRepository.save(updateEntityReturn);
				//Thread.sleep(2000L);
				
				//updateEntityReturn = this.findExamById(updateEntityReturn.getExam_ID());
				  
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			
			ModelMapper modelMapper1 = new ModelMapper();
			modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT); // should match strictly
			examResponseUpdateModel = modelMapper.map(updateEntityReturn, ExamResponseUpdateModel.class);
		}
		
		return examResponseUpdateModel;
		 
	  
	}
	
	public boolean deleteExam(int examID) {
		  
		boolean isSuccess = false;
		
		  try { 
			  
			  	//examRepository.deleteById(examID); 
			  
			  	ExamEntity examEntity = examRepository.findById(examID).orElse(null);
				if (examEntity == null) {
					return false;
				}
				examEntity.setDeletedFlag(1);
				examRepository.save(examEntity);
				
			  
			  //Thread.sleep(2000L); 
			  
			  isSuccess = true;
			  
		  } catch (Exception e) { // TODO Auto-generated catch block
			  
			  e.printStackTrace(); 
			  
			  isSuccess = false;
		  }
		 
		return isSuccess;
	}
	
	public ExamEntity findExamById(int examID) {
		
		ExamEntity examEntity = examRepository.findById(examID).orElse(null);
		  
		return examEntity;
		  
	}
	 
	
	public List<ExamEntity> findAllExams(){
		
		List<ExamEntity> examsEntity = new ArrayList<ExamEntity>();
		
		examRepository.findAll().forEach(exam -> examsEntity.add(exam));
		
		return examsEntity;
	}
	
	/*
	// Pagination
	public List<ExamEntity> findAllExamsPagination(int pageNumber, int pageSize, String sortedBy, String ascDec){
		
		Pageable paging = PageRequest.of(pageNumber, pageSize, Sort.by(sortedBy).ascending());
		
		
		if(ascDec.compareTo("asc") == 0) {
			
			paging = PageRequest.of(pageNumber, pageSize, Sort.by(sortedBy).ascending());
			
		}else {
			
			paging = PageRequest.of(pageNumber, pageSize, Sort.by(sortedBy).descending());
			
		}
		
		Slice<ExamEntity> slicedResult = examRepository.findAll(paging);
		
		List<ExamEntity> examList = slicedResult.getContent();
		
		if(!examList.isEmpty()) {
			
			return examList;
			
		}else {
			
			return new ArrayList<ExamEntity>();
			
		}
		
	}
	
	*/
	public long getExamsCount() {
		return examRepository.count();
	}
	
	/*
	// Pagination return Page<ExamEntity
	//public Page<ExamEntity> findAllExamsPagination1(int pageNumber, int pageSize, String sortedBy, String ascDec){
	public Page<ExamResponseModelWithClientNames> findAllExamsPagination1(int pageNumber, int pageSize, String sortedBy, String ascDec){
		
		PageRequest paging = PageRequest.of(pageNumber, pageSize, Sort.by(sortedBy).ascending());
		
		
		if(ascDec.compareTo("asc") == 0) {
			
			paging = PageRequest.of(pageNumber, pageSize, Sort.by(sortedBy).ascending());
			
		}else {
			
			paging = PageRequest.of(pageNumber, pageSize, Sort.by(sortedBy).descending());
			
		}
		
		//Page<ExamResponseModelWithClientNames> x = entityToDto(paging);
		
		return entityToDto(paging);
		//return examRepository.findAll(paging);
		
	}
	private Page<ExamResponseModelWithClientNames> entityToDto(PageRequest paging) {
		
		Page<ExamEntity> examEntityPage = examRepository.findAll(paging);
		
		Page<ExamResponseModelWithClientNames> examDtoPage = examEntityPage.map(entity -> {
			ExamResponseModelWithClientNames dto = examEntityToDto(entity);
	        return dto;
	    });
		return examDtoPage;
	}
	private ExamResponseModelWithClientNames examEntityToDto(ExamEntity entity){
		
		ExamResponseModelWithClientNames examDto =  new ExamResponseModelWithClientNames();
		examDto.setExamID(entity.getExamID());
		examDto.setClientID(entity.getClientID());
		examDto.setClientName("");
		examDto.setExamName(entity.getExamName());
		examDto.setExamType(entity.getExamType());
		examDto.setCreatedDate(entity.getCreatedDate());
		examDto.setModifiedDate(entity.getModifiedDate());
		
	    return examDto;
	}
	*/
	
	
	// Version 1 - Working without Client Names
	public Page<ExamEntity> findAllExamsPagination1(int pageNumber, int pageSize, String sortedBy, String ascDec){
			
			PageRequest paging = PageRequest.of(pageNumber, pageSize, Sort.by(sortedBy).ascending());
			
			
			if(ascDec.compareTo("asc") == 0) {
				
				paging = PageRequest.of(pageNumber, pageSize, Sort.by(sortedBy).ascending());
				
			}else {
				
				paging = PageRequest.of(pageNumber, pageSize, Sort.by(sortedBy).descending());
				
			}
			
			//return examRepository.findAll(paging);
			return examRepository.findAllByDeletedFlag(paging, 0);
			
	}
	
	// Added by Bert on 27Jan2022
	// For Client-Parent-Child UI (Certification Pathway Details)
	public List<ExamEntity> findAllExamsByClientIDAndDeletedFlag(int clientID) { 
		
		List<ExamEntity> clientExamsList = this.examRepository.findAllByClientIDAndDeletedFlag(clientID, 0);
		
		return clientExamsList;
	}
	
	
	/*
	public ExamResponseModelWithClientNames findAllExamsPagination1(int pageNumber, int pageSize, String sortedBy, String ascDec){
		
		PageRequest paging = PageRequest.of(pageNumber, pageSize, Sort.by(sortedBy).ascending());
		
		
		if(ascDec.compareTo("asc") == 0) {
			
			paging = PageRequest.of(pageNumber, pageSize, Sort.by(sortedBy).ascending());
			
		}else {
			
			paging = PageRequest.of(pageNumber, pageSize, Sort.by(sortedBy).descending());
			
		}
		
		Page<ExamEntity> examEntityPage = examRepository.findAll(paging);
		
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STANDARD); // should match strictly
		examResponseModelWithClientNames = modelMapper.map(examEntityPage, ExamResponseModelWithClientNames.class);
		
		return examResponseModelWithClientNames;
		
	}
	*/
	
	
//	Claude: 02/07/2022 find exam by client id
	public Page<ExamEntity> findByClientID(int pageNumber, int pageSize, String sortedBy, String ascDec, int id) {
		PageRequest paging = PageRequest.of(pageNumber, pageSize, Sort.by(sortedBy).ascending());

		if (ascDec.equals("asc")) {
			paging = PageRequest.of(pageNumber, pageSize, Sort.by(sortedBy).ascending());
		} else {
			paging = PageRequest.of(pageNumber, pageSize, Sort.by(sortedBy).descending());
		}

		return examRepository.findByClientID(paging, id);
	}
	// Added by Bert on 2Feb2022
	// For Client-Parent-Child UI (Exam , Exam Group)
	public Page<ExamEntity> findAllExamsPaginationByClientID(int pageNumber, int pageSize, String sortedBy, String ascDec, int clientID){
		
		PageRequest paging = PageRequest.of(pageNumber, pageSize, Sort.by(sortedBy).ascending());
		
		
		if(ascDec.compareTo("asc") == 0) {
			
			paging = PageRequest.of(pageNumber, pageSize, Sort.by(sortedBy).ascending());
			
		}else {
			
			paging = PageRequest.of(pageNumber, pageSize, Sort.by(sortedBy).descending());
			
		}
		
		return examRepository.findAllByClientIDAndDeletedFlag(paging, clientID, 0);
	}
	
}
