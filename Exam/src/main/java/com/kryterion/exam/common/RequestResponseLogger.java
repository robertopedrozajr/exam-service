package com.kryterion.exam.common;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kryterion.exam.controller.ExamController;
import com.kryterion.exam.entity.ExamEntity;

@Service
public class RequestResponseLogger {
	
	//Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private static final Logger LOG = LogManager.getLogger(RequestResponseLogger.class);
	
	public void log(String label, Object object) {
		
		ObjectMapper Obj = new ObjectMapper();
		
		 String jsonStr = "";
		 
		try {
            jsonStr = Obj.writeValueAsString(object);
            LOG.info(label + " " + jsonStr);
        }
        catch (IOException e) {
        	LOG.error(e.getLocalizedMessage());
        }
		
	}

}
