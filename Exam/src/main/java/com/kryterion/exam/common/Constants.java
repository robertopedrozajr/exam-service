package com.kryterion.exam.common;

import org.springframework.stereotype.Service;

@Service
public class Constants {
	
	public String SAVE_EXAM = "Save Exam";
	
	public String UPDATE_EXAM = "Update Exam";
	
	public String DELETE_EXAM = "Delete Exam";
	
	public String FIND_ALL_EXAMS = "Find All Exams";
	
	public String FIND_EXAM_BY_ID = "Find Exam By ID";
	
	public String FIND_EXAM_PAGINATION = "Find Exam Pagination";
	
	public String FIND_ALL_EXAMS_BY_CLIENT_ID = "Find All Exams By Client ID";

}
