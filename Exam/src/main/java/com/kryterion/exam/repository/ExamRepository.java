package com.kryterion.exam.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kryterion.exam.entity.ExamEntity;
import com.kryterion.exam.model.ExamResponseModelWithClientNames;

@Repository
public interface ExamRepository extends CrudRepository<ExamEntity, Integer> {
	
	public Page<ExamEntity> findAll(Pageable paging); 
	
	public Page<ExamEntity> findAllByDeletedFlag(Pageable paging, int deletedFlag);
	
	// Added by Bert on 27Jan2022
	// For Client-Parent-Child UI (Certification Pathway Details)
	public List<ExamEntity> findAllByClientIDAndDeletedFlag(int clientID, int deletedFlag);
	
	// Added by Bert on 2Feb2022
	// For Client-Parent-Child UI (Exam, Exam Group)
	public Page<ExamEntity> findAllByClientIDAndDeletedFlag(Pageable paging, int clientID, int deletedFlag);
	
	Page<ExamEntity> findByClientID(Pageable paging, int ClientId);
	//public Page<ExamResponseModelWithClientNames> findAll(Pageable paging); 
	
	
}