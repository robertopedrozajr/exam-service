package com.kryterion.exam.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.kryterion.exam.entity.ExamEntity;

public interface ExamRepositoryPageable extends PagingAndSortingRepository<ExamEntity,Integer>{

	Page<ExamEntity> findAll(Pageable paging);

}
