package com.kryterion.exam.entity;

import java.io.Serializable;
import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.beans.factory.annotation.Required;

@Entity
@Table(name = "exam", indexes = { @Index( name = "i_exam_ID", columnList="exam_ID", unique = true )})
public class ExamEntity implements Serializable{
	
	private static final long serialVersionUID = 6885273875156861311L;
	
	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@GeneratedValue(generator = "sequence-generator")
    @GenericGenerator(
      name = "sequence-generator",
      strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
      parameters = {
        @Parameter(name = "sequence_name", value = "user_sequence"),
        @Parameter(name = "initial_value", value = "1"),
        @Parameter(name = "increment_size", value = "1")
        }
    )
	@Column(name = "exam_ID",  unique = true)
	private int examID;
	
	@Column(name = "client_ID", nullable = false)
	private int clientID; // client(FK)
	
	@Column(name = "exam_name", length = 80, nullable = false)
	private String examName;
	
	@Column(name = "exam_type", length = 15, nullable = false)
	private String examType;
	
	@CreationTimestamp
	@Column(name = "created_date", updatable = false)
	//@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date createdDate;
	
	@UpdateTimestamp
	@Column(name = "modified_date", nullable = true, updatable = true)
	private Date modifiedDate;
	
	// Additional Jan 15 2022
		@Column(name = "deleted_flag", length = 3)
		private int deletedFlag = 0;
		public int getDeletedFlag() {
			return deletedFlag;
		}
		public void setDeletedFlag(int deletedFlag) {
			this.deletedFlag = deletedFlag;
		}

	public int getExamID() {
		return examID;
	}

	public void setExamID(int examID) {
		this.examID = examID;
	}

	public int getClientID() {
		return clientID;
	}

	public void setClientID(int clientID) {
		this.clientID = clientID;
	}

	public String getExamName() {
		return examName;
	}

	public void setExamName(String examName) {
		this.examName = examName;
	}

	public String getExamType() {
		return examType;
	}

	public void setExamType(String examType) {
		this.examType = examType;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	/*
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(unique = true)
	private int exam_ID;
	
	@Column(nullable = false)
	private int client_ID; // client(FK)
	
	@Column(length = 80, nullable = false)
	private String exam_name;
	
	@Column(length = 15, nullable = false)
	private String exam_type;
	
	@CreationTimestamp
	@Column(updatable = false)
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date created_date;
	
	@UpdateTimestamp
	@Column(nullable = true, updatable = true)
	private Date modified_date;

	// Getters & Setters
	public int getExam_ID() {
		return exam_ID;
	}

	public void setExam_ID(int exam_ID) {
		this.exam_ID = exam_ID;
	}

	public int getClient_ID() {
		return client_ID;
	}

	public void setClient_ID(int client_ID) {
		this.client_ID = client_ID;
	}

	public String getExam_name() {
		return exam_name;
	}

	public void setExam_name(String exam_name) {
		this.exam_name = exam_name;
	}

	public String getExam_type() {
		return exam_type;
	}

	public void setExam_type(String exam_type) {
		this.exam_type = exam_type;
	}

	public Date getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	public Date getModified_date() {
		return modified_date;
	}

	public void setModified_date(Date modified_date) {
		this.modified_date = modified_date;
	}
	*/

}
