package com.kryterion.exam.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.kryterion.exam.model.ApiError;
import com.kryterion.exam.model.Response;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
    
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(
        HttpMessageNotReadableException e, HttpHeaders headers,
        HttpStatus status, WebRequest request) {
    	
    	Response response = new Response();
		ApiError apiError = new ApiError();
		response.setTimestamp(new Date());
		
    	String errorMessage = e.getMessage().toString();
		Integer errorCode = status.value();
		apiError.setMessage(errorMessage);
		apiError.setCode(errorCode);
		apiError.setTarget("Client"); //static for now
		apiError.setDetails(errorMessage);
		response.setStatus(errorCode);
		response.setMessage(errorMessage);
		response.setError(apiError);
		
		return new ResponseEntity<Object>(response, status);
    }
}