package com.kryterion.exam.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.util.ArrayUtils;
import org.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kryterion.exam.common.RequestResponseLogger;
import com.kryterion.exam.client.rabbitmq.config.ExamClientRabbitMQConfiguration;
import com.kryterion.exam.client.rabbitmq.service.ExamClientRabbitMQSendRequestService;
import com.kryterion.exam.common.Constants;
import com.kryterion.exam.entity.ExamEntity;
import com.kryterion.exam.model.ApiError;
import com.kryterion.exam.model.ExamClientRequestRabbitMQModel;
import com.kryterion.exam.model.ExamClientResponseRabbitMQModel;
import com.kryterion.exam.model.ExamPaginationRequestModel;
import com.kryterion.exam.model.ExamRequestModel;
import com.kryterion.exam.model.ExamRequestUpdateModel;
import com.kryterion.exam.model.ExamResponseModel;
import com.kryterion.exam.model.ExamResponseModelWithClientNames;
import com.kryterion.exam.model.ExamResponseUpdateModel;
import com.kryterion.exam.model.Response;
import com.kryterion.exam.service.ExamService;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping()
public class ExamController {
	
	@Autowired
	ExamClientRequestRabbitMQModel examClientRequestRabbitMQModel; // RabbitMQ
	@Autowired
	ExamClientRabbitMQSendRequestService examClientRabbitMQSendRequestService;  // RabbitMQ
	
	@Autowired
	ExamService examService;
	
	@Autowired
	RequestResponseLogger requestResponseLogger;
	
	@Autowired
	Constants constants;
	
	
	private static final Logger LOG = LogManager.getLogger(ExamController.class);
	
	@Value("${exam.type}")
	private String[] examType;
	
	String logMessage = "";
	
	@Autowired
	ExamPaginationRequestModel examProductPaginationRequestModel;
	
	//@PostMapping("/add")
	//private ResponseEntity<Object> addExam(@RequestBody ExamEntity examEntity) {
	@PostMapping("/add")
	private ResponseEntity<Object> addExam(@RequestBody ExamRequestModel examRequestModel) {
		
		Response response = new Response();
		ApiError apiError = new ApiError();
		response.setTimestamp(new Date());
		String errorMessage = "", errorMessageExamType="";
		int errorCode = 0;
		
		//Logger logger = LoggerFactory.getLogger(this.getClass());
		
		
		try {
			
			int swMarker = 0;
			int swMarkerExamType=0;
			
			if(examRequestModel == null || examRequestModel.getClientID() <= 0) {
				errorMessage = "[Client ID] is required. ";
				swMarker = 1;

			}
			
			
			 if(examRequestModel.getExamName() == null || examRequestModel.getExamName().trim() =="") { 
				 errorMessage = errorMessage+ "[Exam Name] is required. "; 
				 swMarker = 1; 
			 }
			 
			 // VALIDATE EXAM TYPE
			 // ===================================================================================================
			 if(examRequestModel.getExamType() == null || examRequestModel.getExamType().trim() == "") { 
				 errorMessage = errorMessage + "[Exam Type] is required. "; 
				 swMarker = 1; 

			 }
			 
			 
			 if(examRequestModel.getExamType() != (null)) {
				 
				 
				 for(String examType : examType) {
					 
					 if(examType.equals(examRequestModel.getExamType().trim())) {
						 swMarkerExamType = 0;
						 break;
					 }else {
						 
						 swMarkerExamType = 1;
					 }
				 }
				 
				 if( swMarkerExamType == 1) {
					 errorMessageExamType = "Invalid [Exam Type] value " + examRequestModel.getExamType().trim()+".";
				 }
				 
			 }
			// ===================================================================================================
					
			if(swMarker == 1 || swMarkerExamType == 1) {
				
				
				if(swMarkerExamType == 1 && swMarker == 1) {
					errorMessage = errorMessage + errorMessageExamType;
				}else {
					if(swMarkerExamType == 1) {
						errorMessage = errorMessage + errorMessageExamType;
					}/*else {
						errorMessage = errorMessage + "is/are required.";
					}*/
				}
				
				
				//errorMessage = errorMessage + "is/are required. ";
				
				
				errorCode = 400;
				apiError.setMessage(errorMessage);
				apiError.setCode(errorCode);
				apiError.setTarget("Exam");
				apiError.setDetails(errorMessage);
				response.setStatus(errorCode);
				 
				response.setMessage(errorMessage);
				response.setError(apiError);
				
				requestResponseLogger.log("Request " + constants.SAVE_EXAM, examRequestModel); 
				
				logMessage = "[ Error Code:"+ apiError.getCode() + " ] " +
									"[ Error Message: " + apiError.getMessage()+" ] " +			
									"[ Function: Add Exam ] " +
									"[ Target: "+ apiError.getTarget() + "]";
				
				LOG.error(logMessage);
				
				return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);
			}else {
				
				ModelMapper modelMapper = new ModelMapper();
				modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT); // should match strictly
				ExamEntity examEntity = modelMapper.map(examRequestModel, ExamEntity.class);
				
				
				//requestResponseLogger.log("Request " + constants.SAVE_EXAM, examRequestModel); 
				
					ExamEntity result0 = examService.addExam(examEntity);
					
					ModelMapper modelMapperResult = new ModelMapper();
					modelMapperResult.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT); // should match strictly
					ExamResponseModel result = modelMapper.map(result0, ExamResponseModel.class);
					
				//requestResponseLogger.log("Response " + constants.SAVE_EXAM, result);
				
				
				/*
				logger.info("<Request>");
				
					ExamEntity result = examService.addExam(examEntity);
					
					ObjectMapper Obj = new ObjectMapper();
					
					 String jsonStr = "";
					 
					try {
						 
			            jsonStr = Obj.writeValueAsString(result);
			 
			        }
			 
			        catch (IOException e) {
			            e.printStackTrace();
			        }
					 
				logger.info("<Response>" + jsonStr);
				*/
				
				response.setStatus(200);
				response.setMessage("Success");
				response.setData(result);
				
				
				requestResponseLogger.log("Request " + constants.SAVE_EXAM, examRequestModel); 
				
					logMessage = "[ Response: "+ response.getStatus() +" ] " + 
							 "[ Message: " + response.getMessage() +" ] " ;
				
					LOG.info(logMessage);
				
				requestResponseLogger.log("Response " + constants.SAVE_EXAM, response.getData());
				
			}
			
		}catch(Exception e) {
			
			LOG.error(e.getLocalizedMessage());
			
		}
		
		return ResponseEntity.ok(response);
		
	}
	
	//@PutMapping("/update")
	@PutMapping("/update")
	private  ResponseEntity<Object> updateExam(@RequestBody ExamRequestUpdateModel examRequestUpdateModel) {
		
		Response response = new Response();
		ApiError apiError = new ApiError();
		response.setTimestamp(new Date());
		String errorMessage = "", errorMessageExamType="";
		
		int errorCode = 0;
		
		try {
			
			int swMarker = 0;
			int swMarkerExamType = 0;
			
			
			if(examRequestUpdateModel == null || examRequestUpdateModel.getExamID() <= 0) {
				errorMessage = "[Exam ID] is required. ";
				swMarker = 1;
			}
			
			if(examRequestUpdateModel.getClientID() <= 0) {
				//errorMessage = errorMessage + "[Client ID] is zero or less than zero. ";
				errorMessage = errorMessage + "[Client ID] is required. ";
				swMarker = 1;
			}
			
			if(examRequestUpdateModel.getExamName() == null || examRequestUpdateModel.getExamName().trim() == "") {
				errorMessage = errorMessage + "[Client Name] is empty. ";
				swMarker = 1;
			}
			
			if(examRequestUpdateModel.getExamType() == null || examRequestUpdateModel.getExamType().trim() == "") {
				errorMessage = errorMessage + "[Client Type] is empty.";
				swMarker = 1;
			}
			
			 if(examRequestUpdateModel.getExamName() == null || examRequestUpdateModel.getExamName().trim() =="") { 
				 errorMessage = errorMessage+ "[Exam Name] is required."; 
				 swMarker = 1; 
			 }
			 
			 // VALIDATE EXAM TYPE
			 // ===================================================================================================
			 
			 if(examRequestUpdateModel.getExamType() == null || examRequestUpdateModel.getExamType().trim() == "") { 
				 errorMessage = errorMessage + "[Exam Type] is required. "; 
				 swMarker = 1; 

			 }
			 
			 
			 if(examRequestUpdateModel.getExamType() != (null)) {
				 
				 
				 for(String examType : examType) {
					 
					 if(examType.equals(examRequestUpdateModel.getExamType().trim())) {
						 swMarkerExamType = 0;
						 break;
					 }else {
						 
						 swMarkerExamType = 1;
					 }
				 }
				 
				 if( swMarkerExamType == 1) {
					 errorMessageExamType = "Invalid [Exam Type] value " + examRequestUpdateModel.getExamType().trim()+".";
				 }
				 
			 }
			// ===================================================================================================
			
			 //requestResponseLogger.log("Validate" + constants.FIND_EXAM_BY_ID, examRequestUpdateModel); 
			 
			 	ExamEntity examEntity = examService.findExamById(examRequestUpdateModel.getExamID());
			 	
			 //requestResponseLogger.log("Validate Result " + constants.FIND_EXAM_BY_ID, examEntity); 
			 
			
			if(swMarker == 1 || examEntity == null || swMarkerExamType == 1) {
				
				errorMessage = (examEntity == null && !errorMessage.contains("[Exam ID]")) ? errorMessage+" [Exam ID] is not found. ":(errorMessage+="");
				
				if(swMarkerExamType == 1 && swMarker == 1) {
					//errorMessage = errorMessage + "is/are required. " + errorMessageExamType;
					errorMessage = errorMessage + errorMessageExamType;
				}else {
					if(swMarkerExamType == 1) {
						errorMessage = errorMessage + errorMessageExamType;
					}/*else {
						errorMessage = errorMessage + "is/are required.";
					}*/
				}
				
				
				errorCode = 400;
				apiError.setMessage(errorMessage);
				apiError.setCode(errorCode);
				apiError.setTarget("Exam");
				apiError.setDetails(errorMessage);
				response.setStatus(errorCode);
				
				response.setMessage(errorMessage);
				response.setError(apiError);
				
				requestResponseLogger.log("Request " + constants.UPDATE_EXAM, examRequestUpdateModel); 
				
				logMessage = "[ Error Code:"+ apiError.getCode() + " ] " +
									"[ Error Message: " + apiError.getMessage()+" ] " +			
									"[ Function: Update Exam ] " +
									"[ Target: "+ apiError.getTarget() + "]";
				
				LOG.error(logMessage);
				
				return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);
				
			}else {
				
				//requestResponseLogger.log("Request " + constants.UPDATE_EXAM, examRequestUpdateModel); 
				
					ExamResponseUpdateModel result = examService.updateExam(examRequestUpdateModel);
					
				//requestResponseLogger.log("Response " + constants.UPDATE_EXAM, result);
					
				if(result != null) {
					
					response.setStatus(200);
					response.setMessage("Success");
					response.setData(result);
					
					requestResponseLogger.log("Request " + constants.UPDATE_EXAM, examRequestUpdateModel); 
					
						logMessage = "[ Response: "+ response.getStatus() +" ] " + 
							 "[ Message: " + response.getMessage() +" ] " ;
				
						LOG.info(logMessage);
				
					requestResponseLogger.log("Response " + constants.UPDATE_EXAM, response.getData());
					
					
				}else {
					errorCode = 404;
					errorMessage = "Exam ID not found.";
					apiError.setMessage(errorMessage);
					apiError.setCode(errorCode);
					apiError.setTarget("Exam");
					apiError.setDetails(errorMessage);
					response.setStatus(errorCode);
					
					response.setMessage(errorMessage);
					response.setError(apiError);
					
					
					requestResponseLogger.log("Request " + constants.UPDATE_EXAM, examRequestUpdateModel); 
					
					logMessage = "[ Error Code:"+ apiError.getCode() + " ] " +
										"[ Error Message: " + apiError.getMessage()+" ] " +			
										"[ Function: Update Exam ] " +
										"[ Target: "+ apiError.getTarget() + "]";
					
					LOG.error(logMessage);
					
					return new ResponseEntity<Object>(response, HttpStatus.NOT_FOUND);
				}
				
			}
			
			
		}catch(Exception e) {
			
			LOG.error(e.getLocalizedMessage());
			
		}
		
		return ResponseEntity.ok(response);
		
	}
	
	//@DeleteMapping("/delete/{examID}")
	@DeleteMapping("/delete/{examID}")
	private ResponseEntity<Object> deleteExam(@PathVariable int examID) {
		
		Response response = new Response();
		ApiError apiError = new ApiError();
		response.setTimestamp(new Date());
		String errorMessage = "";
		int errorCode = 0;
		
		try {
			ExamEntity examEntity = examService.findExamById(examID);
			if (examID == 0 || examEntity == null) {
				errorMessage = (examEntity == null) ? "Exam ID not found. " + examID:"Exam ID is required.";
				errorCode = 400;
				apiError.setMessage(errorMessage);
				apiError.setCode(errorCode);
				apiError.setTarget("Exam");
				apiError.setDetails(errorMessage);
				response.setStatus(errorCode);
				response.setMessage(errorMessage);
				response.setError(apiError);
				
				requestResponseLogger.log("Request " + constants.DELETE_EXAM, errorMessage); 
				
				logMessage = "[ Error Code:"+ apiError.getCode() + " ] " +
									"[ Error Message: " + apiError.getMessage()+" ] " +			
									"[ Function: Delete Exam ] " +
									"[ Target: "+ apiError.getTarget() + "]";
				
				LOG.error(logMessage);
				
				return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);
			} else {
				boolean result = examService.deleteExam(examID);
				if(result == false) {
					errorMessage = "Failed to delete a exam record with id " + examID;
					errorCode = 405;
					apiError.setMessage(errorMessage);
					apiError.setCode(errorCode);
					apiError.setTarget("Exam");
					apiError.setDetails(errorMessage);
					response.setStatus(errorCode);
					response.setMessage(errorMessage);
					response.setError(apiError);
					
					requestResponseLogger.log("Request " + constants.DELETE_EXAM, errorMessage); 
					
					logMessage = "[ Error Code:"+ apiError.getCode() + " ] " +
										"[ Error Message: " + apiError.getMessage()+" ] " +			
										"[ Function: Delete Exam ] " +
										"[ Target: "+ apiError.getTarget() + "]";
					
					LOG.error(logMessage);
					
					return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);
				}else {
					response.setStatus(200);
					response.setMessage("Exam ID " + examID + " is successfully deleted.");
					response.setData(result);
					
					requestResponseLogger.log("Request " + constants.DELETE_EXAM, response.getMessage()); 
					
						logMessage = "[ Response: "+ response.getStatus() +" ] " + 
							 "[ Message: " + response.getMessage() +" ] " ;
				
						LOG.info(logMessage);
				
					requestResponseLogger.log("Response " + constants.DELETE_EXAM, response.getData());
				}
			}
		} catch (Exception e) {
			errorMessage = e.getMessage().toString();
			errorCode = 405;
			apiError.setMessage(errorMessage);
			apiError.setCode(errorCode);
			apiError.setTarget("Exam");
			apiError.setDetails(errorMessage);
			response.setStatus(errorCode);
			response.setMessage(errorMessage);
			response.setError(apiError);
			
			requestResponseLogger.log("Request " + constants.DELETE_EXAM, errorMessage); 
			
			logMessage = "[ Error Code:"+ apiError.getCode() + " ] " +
								"[ Error Message: " + apiError.getMessage()+" ] " +			
								"[ Function: Delete Exam ] " +
								"[ Target: "+ apiError.getTarget() + "]";
			
			LOG.error(logMessage);
			
			return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);
		}
		
		return ResponseEntity.ok(response);

		
	}
	
	// @GetMapping("/list/{examID}")
	@GetMapping("/find/{examID}")
	private ResponseEntity<Object> findExamByID(@PathVariable int examID) {
		
		//return examService.findExamById(examID);
		
		Response response = new Response();
		ApiError apiError = new ApiError();
		response.setTimestamp(new Date());
		String errorMessage = "";
		int errorCode = 0;

		try {
			ExamEntity examEntity = examService.findExamById(examID);
			if (examID == 0 || examEntity == null) {
				errorMessage = (examEntity == null) ? "Exam ID not found. "+examID:"Exam ID is required.";
				errorCode = 400;
				apiError.setMessage(errorMessage);
				apiError.setCode(errorCode);
				apiError.setTarget("Exam");
				apiError.setDetails(errorMessage);
				response.setStatus(errorCode);
				response.setMessage(errorMessage);
				response.setError(apiError);
				
				requestResponseLogger.log("Request " + constants.FIND_EXAM_BY_ID, apiError.getMessage()); 
				
				logMessage = "[ Error Code:"+ apiError.getCode() + " ] " +
									"[ Error Message: " + apiError.getMessage()+" ] " +			
									"[ Function: Add Exam ] " +
									"[ Target: "+ apiError.getTarget() + "]";
				
				LOG.error(logMessage);
				
				return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);
			} else {
				
				ExamEntity result = examService.findExamById(examID);
				response.setStatus(200);
				response.setMessage("Success");
				response.setData(result);
				
				requestResponseLogger.log("Request " + constants.FIND_EXAM_BY_ID, examID); 
				
				logMessage = "[ Response: "+ response.getStatus() +" ] " + 
						 "[ Message: " + response.getMessage() +" ] " ;
			
				LOG.info(logMessage);
			
				requestResponseLogger.log("Response " + constants.FIND_EXAM_BY_ID, response.getData());
				
			}
		} catch (Exception e) {
			errorMessage = e.getMessage().toString();
			errorCode = 405;
			apiError.setMessage(errorMessage);
			apiError.setCode(errorCode);
			apiError.setTarget("Exam");
			apiError.setDetails(errorMessage);
			response.setStatus(errorCode);
			response.setMessage(errorMessage);
			response.setError(apiError);
			
			requestResponseLogger.log("Request " + constants.FIND_EXAM_BY_ID, examID); 
			
			logMessage = "[ Response: "+ response.getStatus() +" ] " + 
					 "[ Message: " + response.getMessage() +" ] " ;
		
			LOG.info(logMessage);
		
			requestResponseLogger.log("Response " + constants.FIND_EXAM_BY_ID, response.getData());
			
			return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);
		}
		return ResponseEntity.ok(response);
		
	}
	
	@GetMapping("/list")
	private ResponseEntity<Object> findAllExams(){ /* RabbitMQ **/
		
		Response response = new Response();
		ApiError apiError = new ApiError();
		response.setTimestamp(new Date());
		String errorMessage = "";
		int errorCode = 0;
		
		try {
			List<ExamEntity> result = examService.findAllExams();
			response.setStatus(200);
			response.setMessage("Success");
			response.setData(result);
			
			if(result == null || result.size() == 0) {
				/*
				errorMessage = "Not Found – no results are returned even though the request is valid.";
				errorCode = 404;
				apiError.setMessage(errorMessage);
				apiError.setCode(errorCode);
				apiError.setTarget("Exam");
				apiError.setDetails(errorMessage);
				response.setStatus(errorCode);
				response.setMessage(errorMessage);
				response.setError(apiError);
				return new ResponseEntity<Object>(response, HttpStatus.NOT_FOUND);
				*/
				response.setStatus(200);
				response.setMessage("Success");
				response.setData(result);
				
				requestResponseLogger.log("Request " + constants.FIND_ALL_EXAMS, ""); 
				
				logMessage = "[ Response: "+ response.getStatus() +" ] " + 
						 "[ Message: " + response.getMessage() +" ] " ;
			
				LOG.info(logMessage);
			
				requestResponseLogger.log("Response " + constants.FIND_ALL_EXAMS, response.getData());
				
				
				// RabbitMQ
				//examClientRabbitMQSendRequestService.sendRequest(examClientRequestRabbitMQModelList);
				
			}else {
				
				response.setStatus(200);
				response.setMessage("Success");
				response.setData(result);
				
				requestResponseLogger.log("Request " + constants.FIND_ALL_EXAMS, ""); 
				
				logMessage = "[ Response: "+ response.getStatus() +" ] " + 
						 "[ Message: " + response.getMessage() +" ] " ;
			
				LOG.info(logMessage);
			
				requestResponseLogger.log("Response " + constants.FIND_ALL_EXAMS, response.getData());
				
				// RabbitMQ
				
				List<ExamClientRequestRabbitMQModel> examClientRequestRabbitMQModelList = null;
				if(result != null || result.size() != 0) {
					examClientRequestRabbitMQModelList = new ArrayList<ExamClientRequestRabbitMQModel>();
					for(ExamEntity exam : result) {
						ExamClientRequestRabbitMQModel examClientRequestRabbitMQModel = new ExamClientRequestRabbitMQModel();
						examClientRequestRabbitMQModel.setClientId(exam.getClientID());
						
						examClientRequestRabbitMQModelList.add(examClientRequestRabbitMQModel);
					} // populate
				}
				examClientRabbitMQSendRequestService.sendRequest(examClientRequestRabbitMQModelList);
				
				
				/*
				List<Integer> clientIDsList = new ArrayList<Integer>();
				if(result != null || result.size() != 0) {
					for(ExamEntity exam : result) {
						
						clientIDsList.add(exam.getClientID());
						
					} // populate
				}
				examClientRabbitMQSendRequestService.sendRequestClientIDs(clientIDsList);
				*/
				// RabbitMQ
				
			}
		} catch (Exception e) {
			errorMessage = e.getMessage().toString();
			errorCode = 405;
			apiError.setMessage(errorMessage);
			apiError.setCode(errorCode);
			apiError.setTarget("Exam");
			apiError.setDetails(errorMessage);
			response.setStatus(errorCode);
			response.setMessage(errorMessage);
			response.setError(apiError);
			
			requestResponseLogger.log("Request " + constants.FIND_ALL_EXAMS, ""); 
			
			logMessage = "[ Error Code:"+ apiError.getCode() + " ] " +
								"[ Error Message: " + apiError.getMessage()+" ] " +			
								"[ Function: Find All Exam ] " +
								"[ Target: "+ apiError.getTarget() + "]";
			
			LOG.error(logMessage);
			
			return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);
		}
		
		return ResponseEntity.ok(response);
		
	}
	
	// Added by Bert on 27Jan2022
	// For Client-Parent-Child UI (Certification Pathway Details)
	@GetMapping("/list2/{clientID}")
	private ResponseEntity<Object> findAllExamsByClientID(@PathVariable int clientID){
		
		Response response = new Response();
		ApiError apiError = new ApiError();
		response.setTimestamp(new Date());
		String errorMessage = "";
		int errorCode = 0;

		try {
			List<ExamEntity> result = examService.findAllExamsByClientIDAndDeletedFlag(clientID);
			if (clientID <= 0 ) {
				//errorMessage = (result == null) ? "Client ID not found. "+clientID:"Client ID is required.";
				errorMessage = "Invalid Client ID " + clientID;
				errorCode = 400;
				apiError.setMessage(errorMessage);
				apiError.setCode(errorCode);
				apiError.setTarget("Exam");
				apiError.setDetails(errorMessage);
				response.setStatus(errorCode);
				response.setMessage(errorMessage);
				response.setError(apiError);
				
				requestResponseLogger.log("Request " + constants.FIND_ALL_EXAMS_BY_CLIENT_ID, apiError.getMessage()); 
				
				logMessage = "[ Error Code:"+ apiError.getCode() + " ] " +
									"[ Error Message: " + apiError.getMessage()+" ] " +			
									"[ Function: Add Exam ] " +
									"[ Target: "+ apiError.getTarget() + "]";
				
				LOG.error(logMessage);
				
				return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);
			} else {
				
				//ExamEntity result = examService.findExamById(clientID);
				response.setStatus(200);
				response.setMessage("Success");
				response.setData(result);
				
				requestResponseLogger.log("Request " + constants.FIND_ALL_EXAMS_BY_CLIENT_ID, clientID); 
				
				logMessage = "[ Response: "+ response.getStatus() +" ] " + 
						 "[ Message: " + response.getMessage() +" ] " ;
			
				LOG.info(logMessage);
			
				requestResponseLogger.log("Response " + constants.FIND_ALL_EXAMS_BY_CLIENT_ID, response.getData());
				
			}
		} catch (Exception e) {
			errorMessage = e.getMessage().toString();
			errorCode = 405;
			apiError.setMessage(errorMessage);
			apiError.setCode(errorCode);
			apiError.setTarget("Exam");
			apiError.setDetails(errorMessage);
			response.setStatus(errorCode);
			response.setMessage(errorMessage);
			response.setError(apiError);
			
			requestResponseLogger.log("Request " + constants.FIND_ALL_EXAMS_BY_CLIENT_ID, clientID); 
			
			logMessage = "[ Response: "+ response.getStatus() +" ] " + 
					 "[ Message: " + response.getMessage() +" ] " ;
		
			LOG.info(logMessage);
		
			requestResponseLogger.log("Response " + constants.FIND_ALL_EXAMS_BY_CLIENT_ID, response.getData());
			
			return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);
		}
		return ResponseEntity.ok(response);
		
	}
	/*
	//private List<ClientLedgerResponseModel> getAllClientLedgers(
	@GetMapping(value="/page/", params = {"pageNumber","pageSize","sortedBy","ascDesc"})
	private ResponseEntity<Object> findAllExamsPagination(
			@RequestParam(defaultValue = "0") int pageNumber,
			@RequestParam(defaultValue = "10") int pageSize,
			@RequestParam(defaultValue = "examID") String sortedBy,
			@RequestParam(defaultValue = "asc") String ascDesc)
			{
		
		Response response = new Response();
		ApiError apiError = new ApiError();
		response.setTimestamp(new Date());
		String errorMessage = "";
		int errorCode = 0;
		
		try {
			
			
			List<ExamEntity> examEntity = examService.findAllExamsPagination(pageNumber, pageSize, sortedBy, ascDesc);
			
			ModelMapper modelMapper = new ModelMapper();
			modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT); // should match strictly
			List<ExamResponseModel> result = modelMapper.map(examEntity, new TypeToken<List<ExamResponseModel>>() {}.getType());
			
			response.setStatus(200);
			response.setMessage("Success");
			response.setData(result);
			
			
			if(result == null || result.size() == 0) {
				errorMessage = "Not Found – no results are returned even though the request is valid.";
				errorCode = 404;
				apiError.setMessage(errorMessage);
				apiError.setCode(errorCode);
				apiError.setTarget("Exam");
				apiError.setDetails(errorMessage);
				response.setStatus(errorCode);
				response.setMessage(errorMessage);
				response.setError(apiError);
				return new ResponseEntity<Object>(response, HttpStatus.NOT_FOUND);
			}else {
				response.setStatus(200);
				response.setMessage("Success");
				response.setData(result);
			}
			
			
		} catch (Exception e) {
			errorMessage = e.getMessage().toString();
			errorCode = 405;
			apiError.setMessage(errorMessage);
			apiError.setCode(errorCode);
			apiError.setTarget("Exam");
			apiError.setDetails(errorMessage);
			response.setStatus(errorCode);
			response.setMessage(errorMessage);
			response.setError(apiError);
			return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);
		}
		return ResponseEntity.ok(response);
		
	}
	
	*/
	
	/*
	@GetMapping(value="/page/", params = {"pageNumber","pageSize","sortedBy","ascDesc"})
	private ResponseEntity<Object> findAllExamsPagination(
			@RequestParam(defaultValue = "0") int pageNumber,
			@RequestParam(defaultValue = "10") int pageSize,
			@RequestParam(defaultValue = "examID") String sortedBy,
			@RequestParam(defaultValue = "asc") String ascDesc)
			{
		
		Response response = new Response();
		ApiError apiError = new ApiError();
		response.setTimestamp(new Date());
		String errorMessage = "";
		int errorCode = 0;
		
		try {
			
			
			Page<ExamEntity> result = examService.findAllExamsPagination1(pageNumber, pageSize, sortedBy, ascDesc);
			//Page<ExamResponseModelWithClientNames> result = examService.findAllExamsPagination1(pageNumber, pageSize, sortedBy, ascDesc);
			
			examProductPaginationRequestModel.setPageNumber(pageNumber);
			examProductPaginationRequestModel.setPageSize(pageSize);
			examProductPaginationRequestModel.setSortedBy(sortedBy);
			examProductPaginationRequestModel.setAscDesc(ascDesc);

			response.setStatus(200);
			response.setMessage("Success");
			response.setData(result);
			
			
			if(result == null || result.getSize() == 0) {
				
				errorMessage = "Not Found – no results are returned even though the request is valid.";
				errorCode = 404;
				apiError.setMessage(errorMessage);
				apiError.setCode(errorCode);
				apiError.setTarget("Exam");
				apiError.setDetails(errorMessage);
				response.setStatus(errorCode);
				response.setMessage(errorMessage);
				response.setError(apiError);
				
				requestResponseLogger.log("Request " + constants.FIND_EXAM_PAGINATION, examProductPaginationRequestModel); 
				
				logMessage = "[ Error Code:"+ apiError.getCode() + " ] " +
									"[ Error Message: " + apiError.getMessage()+" ] " +			
									"[ Function: Find Exam Pagination ] " +
									"[ Target: "+ apiError.getTarget() + "]";
				
				LOG.error(logMessage);
				
				return new ResponseEntity<Object>(response, HttpStatus.NOT_FOUND);
				
			}else {
				
				response.setStatus(200);
				response.setMessage("Success");
				//response.setData(result);
				
				requestResponseLogger.log("Request " + constants.FIND_EXAM_PAGINATION, examProductPaginationRequestModel); 
				
				logMessage = "[ Response: "+ response.getStatus() +" ] " + 
						 "[ Message: " + response.getMessage() +" ] " ;
			
				LOG.info(logMessage);
			
				requestResponseLogger.log("Response " + constants.FIND_EXAM_PAGINATION, response.getData());
				
				
				
				// RabbitMQ
				
				List<ExamClientRequestRabbitMQModel> examClientRequestRabbitMQModelList = null;
				if(result != null || result.getSize() != 0) {
					examClientRequestRabbitMQModelList = new ArrayList<ExamClientRequestRabbitMQModel>();
					for(ExamEntity exam : result) {
						ExamClientRequestRabbitMQModel examClientRequestRabbitMQModel = new ExamClientRequestRabbitMQModel();
						examClientRequestRabbitMQModel.setClientId(exam.getClientID());
						
						examClientRequestRabbitMQModelList.add(examClientRequestRabbitMQModel);
					} // populate
				}
				examClientRabbitMQSendRequestService.sendRequest(examClientRequestRabbitMQModelList);
				
				System.out.println("Request: " + result.getContent().size());
				Thread.sleep(700); 
				//System.out.println("Response: " + this.examClientResponseRabbitMQModelList.size());

				
					if(this.examClientResponseRabbitMQModelList != null && this.examClientResponseRabbitMQModelList.size() > 0) { 
						
						System.out.println("Response: " + this.examClientResponseRabbitMQModelList.size());
						
						for(ExamClientResponseRabbitMQModel eCResponseList : this.examClientResponseRabbitMQModelList) {
							
							for(ExamEntity eEntity : result) {
								
								if(eCResponseList.getClientId() == eEntity.getClientID()) {
									
									eEntity.setExamName(eEntity.getExamName()+"~"+eCResponseList.getClientName());
									break;
									
								}
									
							}
						}
						
					} 
					
				//System.out.println(result);
				
				
					
			}
			
			
		} catch (Exception e) {
			
			errorMessage = e.getMessage().toString();
			errorCode = 405;
			apiError.setMessage(errorMessage);
			apiError.setCode(errorCode);
			apiError.setTarget("Exam");
			apiError.setDetails(errorMessage);
			response.setStatus(errorCode);
			response.setMessage(errorMessage);
			response.setError(apiError);
			
			requestResponseLogger.log("Request " + constants.FIND_EXAM_PAGINATION, ""); 
			
			logMessage = "[ Error Code:"+ apiError.getCode() + " ] " +
								"[ Error Message: " + apiError.getMessage()+" ] " +			
								"[ Function: Find Exam Pagination ] " +
								"[ Target: "+ apiError.getTarget() + "]";
			
			LOG.error(logMessage);
			
			return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);
		}
		
		return ResponseEntity.ok(response);
		
	}
	*/
	
	@GetMapping(value="/page/", params = {"pageNumber","pageSize","sortedBy","ascDesc"})
	private ResponseEntity<Object> findAllExamsPagination(
			@RequestParam(defaultValue = "0") int pageNumber,
			@RequestParam(defaultValue = "10") int pageSize,
			@RequestParam(defaultValue = "examID") String sortedBy,
			@RequestParam(defaultValue = "asc") String ascDesc)
			{
		
		Response response = new Response();
		ApiError apiError = new ApiError();
		response.setTimestamp(new Date());
		String errorMessage = "";
		int errorCode = 0;
		
		try {
			
			
			Page<ExamEntity> result = examService.findAllExamsPagination1(pageNumber, pageSize, sortedBy, ascDesc);
			
			examProductPaginationRequestModel.setPageNumber(pageNumber);
			examProductPaginationRequestModel.setPageSize(pageSize);
			examProductPaginationRequestModel.setSortedBy(sortedBy);
			examProductPaginationRequestModel.setAscDesc(ascDesc);

			response.setStatus(200);
			response.setMessage("Success");
			response.setData(result);
			
			
			if(result == null || result.getSize() == 0) {
				
				errorMessage = "Not Found – no results are returned even though the request is valid.";
				errorCode = 404;
				apiError.setMessage(errorMessage);
				apiError.setCode(errorCode);
				apiError.setTarget("Exam");
				apiError.setDetails(errorMessage);
				response.setStatus(errorCode);
				response.setMessage(errorMessage);
				response.setError(apiError);
				
				requestResponseLogger.log("Request " + constants.FIND_EXAM_PAGINATION, examProductPaginationRequestModel); 
				
				logMessage = "[ Error Code:"+ apiError.getCode() + " ] " +
									"[ Error Message: " + apiError.getMessage()+" ] " +			
									"[ Function: Find Exam Pagination ] " +
									"[ Target: "+ apiError.getTarget() + "]";
				
				LOG.error(logMessage);
				
				return new ResponseEntity<Object>(response, HttpStatus.NOT_FOUND);
				
			}else {
				
				response.setStatus(200);
				response.setMessage("Success");
				response.setData(result);
				
				requestResponseLogger.log("Request " + constants.FIND_EXAM_PAGINATION, examProductPaginationRequestModel); 
				
				logMessage = "[ Response: "+ response.getStatus() +" ] " + 
						 "[ Message: " + response.getMessage() +" ] " ;
			
				LOG.info(logMessage);
			
				requestResponseLogger.log("Response " + constants.FIND_EXAM_PAGINATION, response.getData());
			}
			
			
		} catch (Exception e) {
			
			errorMessage = e.getMessage().toString();
			errorCode = 405;
			apiError.setMessage(errorMessage);
			apiError.setCode(errorCode);
			apiError.setTarget("Exam");
			apiError.setDetails(errorMessage);
			response.setStatus(errorCode);
			response.setMessage(errorMessage);
			response.setError(apiError);
			
			requestResponseLogger.log("Request " + constants.FIND_EXAM_PAGINATION, ""); 
			
			logMessage = "[ Error Code:"+ apiError.getCode() + " ] " +
								"[ Error Message: " + apiError.getMessage()+" ] " +			
								"[ Function: Find Exam Pagination ] " +
								"[ Target: "+ apiError.getTarget() + "]";
			
			LOG.error(logMessage);
			
			return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);
		}
		return ResponseEntity.ok(response);
		
	}

	@GetMapping("/count")
	private  ResponseEntity<Object> getExamsCount() {
		
		Response response = new Response();
		ApiError apiError = new ApiError();
		response.setTimestamp(new Date());
		String errorMessage = "";
		int errorCode = 0;
		
		try {
			long result = examService.getExamsCount();
			response.setStatus(200);
			response.setMessage("Success");
			response.setData(result);
			if(result == 0) {
				errorMessage = "Not Found – no results are returned even though the request is valid.";
				errorCode = 404;
				apiError.setMessage(errorMessage);
				apiError.setCode(errorCode);
				apiError.setTarget("Exam");
				apiError.setDetails(errorMessage);
				response.setStatus(errorCode);
				response.setMessage(errorMessage);
				response.setError(apiError);
				return new ResponseEntity<Object>(response, HttpStatus.NOT_FOUND);
			}else {
				response.setStatus(200);
				response.setMessage("Success");
				response.setData(result);
			}
		} catch (Exception e) {
			errorMessage = e.getMessage().toString();
			errorCode = 405;
			apiError.setMessage(errorMessage);
			apiError.setCode(errorCode);
			apiError.setTarget("Exam");
			apiError.setDetails(errorMessage);
			response.setStatus(errorCode);
			response.setMessage(errorMessage);
			response.setError(apiError);
			return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);
		}
		return ResponseEntity.ok(response);

	}
	@GetMapping("/find/by/clientId")
	private  ResponseEntity<Object> findByExamClientId(
			@RequestParam(defaultValue = "0") int pageNumber,
			@RequestParam(defaultValue = "10") int pageSize,
			@RequestParam(defaultValue = "examID") String sortedBy,
			@RequestParam(defaultValue = "asc") String ascDesc,
			@RequestParam int clientID) {
		
		Response response = new Response();
		ApiError apiError = new ApiError();
		response.setTimestamp(new Date());
		String errorMessage = "";
		int errorCode = 0;
		
		try {
			Page<ExamEntity> result = examService.findByClientID(pageNumber,pageSize,sortedBy,ascDesc,clientID);
			response.setStatus(200);
			response.setMessage("Success");
			response.setData(result);
			if(result == null || result.getSize() == 0) {
				errorMessage = "Not Found – no results are returned even though the request is valid.";
				errorCode = 404;
				apiError.setMessage(errorMessage);
				apiError.setCode(errorCode);
				apiError.setTarget("Exam");
				apiError.setDetails(errorMessage);
				response.setStatus(errorCode);
				response.setMessage(errorMessage);
				response.setError(apiError);
				return new ResponseEntity<Object>(response, HttpStatus.NOT_FOUND);
			}else {
				response.setStatus(200);
				response.setMessage("Success");
				response.setData(result);
			}
		} catch (Exception e) {
			errorMessage = e.getMessage().toString();
			errorCode = 405;
			apiError.setMessage(errorMessage);
			apiError.setCode(errorCode);
			apiError.setTarget("Exam");
			apiError.setDetails(errorMessage);
			response.setStatus(errorCode);
			response.setMessage(errorMessage);
			response.setError(apiError);
			return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);
		}
		return ResponseEntity.ok(response);

	}

	/*
	// Test Resilience4J Circuit Breaker
	
	private static final String TEST_SERVICE = "testService";
	
	@Autowired
	private RestTemplate restTemplate;
	
	
	@GetMapping("/test")
	@CircuitBreaker(name=TEST_SERVICE, fallbackMethod = "testServiceFallback")
	public ResponseEntity<String> test(){
		
		String response = restTemplate.getForObject("http://localhost:8096/list", String.class);
		
		return new ResponseEntity<String>(response, HttpStatus.OK);
		
	}
	
	public ResponseEntity<String> testServiceFallback(Exception e){
		
		return new ResponseEntity<String>("Test service is down", HttpStatus.OK);
		
	}
	*/
	
	
	// RabbitMQ
	List<ExamClientResponseRabbitMQModel> examClientResponseRabbitMQModelList;
	@RabbitListener(queues = ExamClientRabbitMQConfiguration.QUEUE_CLIENT_EXAM)
	private void listenToClientResponse(List<ExamClientResponseRabbitMQModel> examClientResponseRabbitMQModelList) {
		
		//if(examClientResponseRabbitMQModelList != null || examClientResponseRabbitMQModelList.size() > 0 ) {
			
			// Test - populate response from GetClientNamesService
			for(ExamClientResponseRabbitMQModel examClientInfo : examClientResponseRabbitMQModelList) {
				System.out.println("Client ID  : " + examClientInfo.getClientId() + " " + "Client Name: " + examClientInfo.getClientName());
			}
			this.examClientResponseRabbitMQModelList = new ArrayList<ExamClientResponseRabbitMQModel>();
			this.examClientResponseRabbitMQModelList = examClientResponseRabbitMQModelList;
		//}
		
	} 

	
	// Added by Bert on 2Feb2022
		// For Client-Parent-Child UI (Exam , Exam Group)
	@GetMapping(value="/page-client/", params = {"pageNumber","pageSize","sortedBy","ascDesc", "clientID"})
	private ResponseEntity<Object> findAllExamsPaginationByClientId(
			@RequestParam(defaultValue = "0") int pageNumber,
			@RequestParam(defaultValue = "10") int pageSize,
			@RequestParam(defaultValue = "examID") String sortedBy,
			@RequestParam(defaultValue = "asc") String ascDesc,
			@RequestParam(defaultValue = "0") int clientID)
			{
		
		Response response = new Response();
		ApiError apiError = new ApiError();
		response.setTimestamp(new Date());
		String errorMessage = "";
		int errorCode = 0;
		
		try {
			
			
			Page<ExamEntity> result = examService.findAllExamsPaginationByClientID(pageNumber, pageSize, sortedBy, ascDesc, clientID);
			
			examProductPaginationRequestModel.setPageNumber(pageNumber);
			examProductPaginationRequestModel.setPageSize(pageSize);
			examProductPaginationRequestModel.setSortedBy(sortedBy);
			examProductPaginationRequestModel.setAscDesc(ascDesc);

			response.setStatus(200);
			response.setMessage("Success");
			response.setData(result);
			
			
			if(result == null || result.getSize() == 0) {
				
				errorMessage = "Not Found – no results are returned even though the request is valid.";
				errorCode = 404;
				apiError.setMessage(errorMessage);
				apiError.setCode(errorCode);
				apiError.setTarget("Exam");
				apiError.setDetails(errorMessage);
				response.setStatus(errorCode);
				response.setMessage(errorMessage);
				response.setError(apiError);
				
				requestResponseLogger.log("Request " + constants.FIND_EXAM_PAGINATION, examProductPaginationRequestModel); 
				
				logMessage = "[ Error Code:"+ apiError.getCode() + " ] " +
									"[ Error Message: " + apiError.getMessage()+" ] " +			
									"[ Function: Find Exam Pagination By Client Id] " +
									"[ Target: "+ apiError.getTarget() + "]";
				
				LOG.error(logMessage);
				
				return new ResponseEntity<Object>(response, HttpStatus.NOT_FOUND);
				
			}else {
				
				response.setStatus(200);
				response.setMessage("Success");
				response.setData(result);
				
				requestResponseLogger.log("Request " + constants.FIND_EXAM_PAGINATION, examProductPaginationRequestModel); 
				
				logMessage = "[ Response: "+ response.getStatus() +" ] " + 
						 "[ Message: " + response.getMessage() +" ] " ;
			
				LOG.info(logMessage);
			
				requestResponseLogger.log("Response " + constants.FIND_EXAM_PAGINATION, response.getData());
			}
			
			
		} catch (Exception e) {
			
			errorMessage = e.getMessage().toString();
			errorCode = 405;
			apiError.setMessage(errorMessage);
			apiError.setCode(errorCode);
			apiError.setTarget("Exam");
			apiError.setDetails(errorMessage);
			response.setStatus(errorCode);
			response.setMessage(errorMessage);
			response.setError(apiError);
			
			requestResponseLogger.log("Request " + constants.FIND_EXAM_PAGINATION, ""); 
			
			logMessage = "[ Error Code:"+ apiError.getCode() + " ] " +
								"[ Error Message: " + apiError.getMessage()+" ] " +			
								"[ Function: Find Exam Pagination By Client Id] " +
								"[ Target: "+ apiError.getTarget() + "]";
			
			LOG.error(logMessage);
			
			return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);
		}
		return ResponseEntity.ok(response);
		
	}
}

// PAGINATION ORIGINAL WORKING - WITHOUT RABBITMQ 31-Decebmer-2021
/*
 

	@GetMapping(value="/page/", params = {"pageNumber","pageSize","sortedBy","ascDesc"})
	private ResponseEntity<Object> findAllExamsPagination(
			@RequestParam(defaultValue = "0") int pageNumber,
			@RequestParam(defaultValue = "10") int pageSize,
			@RequestParam(defaultValue = "examID") String sortedBy,
			@RequestParam(defaultValue = "asc") String ascDesc)
			{
		
		Response response = new Response();
		ApiError apiError = new ApiError();
		response.setTimestamp(new Date());
		String errorMessage = "";
		int errorCode = 0;
		
		try {
			
			
			Page<ExamEntity> result = examService.findAllExamsPagination1(pageNumber, pageSize, sortedBy, ascDesc);
			
			examProductPaginationRequestModel.setPageNumber(pageNumber);
			examProductPaginationRequestModel.setPageSize(pageSize);
			examProductPaginationRequestModel.setSortedBy(sortedBy);
			examProductPaginationRequestModel.setAscDesc(ascDesc);

			response.setStatus(200);
			response.setMessage("Success");
			response.setData(result);
			
			
			if(result == null || result.getSize() == 0) {
				
				errorMessage = "Not Found – no results are returned even though the request is valid.";
				errorCode = 404;
				apiError.setMessage(errorMessage);
				apiError.setCode(errorCode);
				apiError.setTarget("Exam");
				apiError.setDetails(errorMessage);
				response.setStatus(errorCode);
				response.setMessage(errorMessage);
				response.setError(apiError);
				
				requestResponseLogger.log("Request " + constants.FIND_EXAM_PAGINATION, examProductPaginationRequestModel); 
				
				logMessage = "[ Error Code:"+ apiError.getCode() + " ] " +
									"[ Error Message: " + apiError.getMessage()+" ] " +			
									"[ Function: Find Exam Pagination ] " +
									"[ Target: "+ apiError.getTarget() + "]";
				
				LOG.error(logMessage);
				
				return new ResponseEntity<Object>(response, HttpStatus.NOT_FOUND);
				
			}else {
				
				response.setStatus(200);
				response.setMessage("Success");
				response.setData(result);
				
				requestResponseLogger.log("Request " + constants.FIND_EXAM_PAGINATION, examProductPaginationRequestModel); 
				
				logMessage = "[ Response: "+ response.getStatus() +" ] " + 
						 "[ Message: " + response.getMessage() +" ] " ;
			
				LOG.info(logMessage);
			
				requestResponseLogger.log("Response " + constants.FIND_EXAM_PAGINATION, response.getData());
			}
			
			
		} catch (Exception e) {
			
			errorMessage = e.getMessage().toString();
			errorCode = 405;
			apiError.setMessage(errorMessage);
			apiError.setCode(errorCode);
			apiError.setTarget("Exam");
			apiError.setDetails(errorMessage);
			response.setStatus(errorCode);
			response.setMessage(errorMessage);
			response.setError(apiError);
			
			requestResponseLogger.log("Request " + constants.FIND_EXAM_PAGINATION, ""); 
			
			logMessage = "[ Error Code:"+ apiError.getCode() + " ] " +
								"[ Error Message: " + apiError.getMessage()+" ] " +			
								"[ Function: Find Exam Pagination ] " +
								"[ Target: "+ apiError.getTarget() + "]";
			
			LOG.error(logMessage);
			
			return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);
		}
		return ResponseEntity.ok(response);
		
	}
	
 
 */
